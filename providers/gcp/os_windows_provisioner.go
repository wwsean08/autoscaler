package gcp

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"google.golang.org/api/compute/v1"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/backoff"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/windows"
)

const (
	windowsSysprepSpecializeMessage    = "GCEInstanceSetup: Starting sysprep specialize phase"
	windowsActivationSuccessfulMessage = "GCEInstanceSetup: Activation successful"
	windowsFinishedStartupScripts      = "GCEMetadataScripts: Finished running startup scripts"
)

func (p *Provider) provisionWindows(ctx context.Context, logger logging.Logger, vm *config.VirtualMachine, instance *compute.Instance) error {
	log := logger.WithField("os", osWindows)
	log.Info("Provisioning Windows OS...")

	password, err := p.getWindowsPassword(ctx, log, vm, instance)
	if err != nil {
		return fmt.Errorf("couldn't get Windows password for the %q instance: %w", vm.Name, err)
	}

	logger.Info("Got Windows password")

	vm.Username = p.config.Username
	vm.Password = password

	err = p.waitForWindowsReadiness(ctx, log, vm)
	if err != nil {
		return fmt.Errorf("couldn't ensure that Windows is ready for the %q instance: %w", vm.Name, err)
	}

	log.Info("Windows OS provisioned")

	return nil
}

func (p *Provider) getWindowsPassword(ctx context.Context, logger logging.Logger, vm *config.VirtualMachine, instance *compute.Instance) (string, error) {
	logger.Info("Getting Windows password...")

	instances, err := p.getInstancesService(ctx)
	if err != nil {
		return "", err
	}

	credentials, err := windows.NewCredentials(p.config.Username)
	if err != nil {
		return "", fmt.Errorf("couldn't prepare Windows key info: %w", err)
	}

	err = p.setWindowsPasswordMetadata(ctx, logger, vm, instance, credentials)
	if err != nil {
		return "", fmt.Errorf("couldn't update Virtual Machine metadata with Windows keys: %w", err)
	}

	b := backoff.Settings{
		InitialInterval:     15,
		RandomizationFactor: 0.1,
		Multiplier:          1.25,
		MaxInterval:         30,
		MaxElapsedTime:      300,
	}
	err = backoff.Loop(b, logger, func(logger logging.Logger) (bool, error) {
		logger.Info("Reading Virtual Machine password from the serial console...")

		err = p.readEncryptedPasswordFromSerialConsole(ctx, instances, vm, credentials)
		if err == nil {
			return true, nil
		}

		logger.
			WithError(err).
			Debug("couldn't get password")

		return false, nil
	})

	if err != nil {
		return "", fmt.Errorf("couldn't read encrypted password from serial console: %w", err)
	}

	logger.Info("Decoding and decrypting Virtual Machine password...")

	return credentials.DecodeAndDecryptPassword()
}

func (p *Provider) setWindowsPasswordMetadata(ctx context.Context, logger logging.Logger, vm *config.VirtualMachine, instance *compute.Instance, credentials *windows.Credentials) error {
	windowsKey, err := credentials.WindowsKey()
	if err != nil {
		return fmt.Errorf("couldn't encode Windows key info: %w", err)
	}

	metadata := instance.Metadata

	found := false
	for _, item := range metadata.Items {
		if item.Key != windowsKeysMetadataKey {
			continue
		}

		val := fmt.Sprintf("%s\n%s", *item.Value, windowsKey)
		item.Value = &val
		found = true

		break
	}

	if !found {
		metadata.Items = append(metadata.Items, &compute.MetadataItems{
			Key:   windowsKeysMetadataKey,
			Value: &windowsKey,
		})
	}

	return p.updateMetadata(ctx, logger, *vm, metadata)
}

func (p *Provider) readEncryptedPasswordFromSerialConsole(ctx context.Context, instances *compute.InstancesService, vm *config.VirtualMachine, credentials *windows.Credentials) error {
	o, err := instances.
		GetSerialPortOutput(p.config.Project, p.config.Zone, vm.Name).
		Port(serialConsolePassword).
		Context(ctx).
		Do()

	if err != nil {
		return fmt.Errorf("couldn't get output from Serial Port 4 from %q instance: %w", vm.Name, err)
	}

	for _, line := range strings.Split(o.Contents, "\n") {
		found, err := credentials.ScanEncryptedPassword([]byte(line))
		if err != nil {
			return err
		}

		if found {
			return nil
		}
	}

	return errors.New("password not found in Serial Console output")
}

func (p *Provider) waitForWindowsReadiness(ctx context.Context, logger logging.Logger, vm *config.VirtualMachine) error {
	instances, err := p.getInstancesService(ctx)
	if err != nil {
		return err
	}

	b := backoff.Settings{
		InitialInterval:     30,
		RandomizationFactor: 0.1,
		Multiplier:          1.25,
		MaxInterval:         120,
		MaxElapsedTime:      600,
	}
	err = backoff.Loop(b, logger, func(logger logging.Logger) (bool, error) {
		logger.Info("Checking Windows Virtual Machine readiness")

		o, err := instances.
			GetSerialPortOutput(p.config.Project, p.config.Zone, vm.Name).
			Port(serialConsoleReadiness).
			Context(ctx).
			Do()

		if err == nil && readinessDetected(o.Contents) {
			logger.Debug("Windows Virtual Machine is ready")

			return true, nil
		}

		return false, err
	})

	if err != nil {
		return fmt.Errorf("Windows Virtual Machine %q is not ready: %w", vm.Name, err)
	}

	return nil
}

//
// At least for Windows, there are two ways how the machine may be started:
//
// 1. The machine is created and booted. After boot, GCP's "agent" is started
//    and it enters a `sysprep` stage. At this moment a user defined script
//    may be executed. After the system preparation is done, VM IS REBOOTED.
//    When it's started again, then the GCP agent is also started. This is another
//    moment when user-defined script may be executed. After this system startup
//    step is done, machine should be fully accessible.
//
//    In this case, in the first boot we can find the message:
//    `GCEInstanceSetup: Starting sysprep specialize phase`
//    and the full startup is ended with: `GCEInstanceSetup: Activation successful`.
//
//    From what we've found, this startup sequence exists when the official Windows
//    images in GCP are used and machine is started for the first time.
//
// 2. The machine is created and booted. After boot, GCP's "agent" is started,
//    but in the normal `startup` stage. No `sysprep` scripts are handled. The VM
//    is also not rebooted. If user defined a startup script, then it's executed at
//    this time.
//
//    In this case, there is only one boot, and the initialization is ended with:
//    "GCEMetadataScripts: Finished running startup scripts".
//
//    We've found, that this sequence is used when we've used our own image, built
//    with Packer on top of one of the official ones.
//
//    Packer builds the image by creating a VM, provisioning the OS with all user
//    definitions and finally creating the image from the disk of a stopped VM.
//    This probably causes the `sysprep` to be executed during Packer build.
//    The VM boot from such image uses an "existing" VM (because of the previously
//    started VM disk being the source) which is already activated and doesn't trigger
//    the `sysprep` boot.
//
// Because we want to support both type of images, the method bellow tries to recognize
// if the `sysprep` stage was started. Depending on this, it looks for the proper end
// message.
//
func readinessDetected(content string) bool {
	if strings.Contains(content, windowsSysprepSpecializeMessage) {
		return strings.Contains(content, windowsActivationSuccessfulMessage)
	}

	return strings.Contains(content, windowsFinishedStartupScripts)
}
