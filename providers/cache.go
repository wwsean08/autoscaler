package providers

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/encoding"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/fs"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
)

const (
	cacheFileMode os.FileMode = 0600
)

type CacheStore interface {
	Read(vmName string) (config.VirtualMachine, error)
	Write(vm config.VirtualMachine) error
	Delete(vm config.VirtualMachine) error
}

type fsCacheStore struct {
	directory string
	fs        fs.FS
	encoder   encoding.Encoder
}

func newFSCacheStorage(directory string, f fs.FS, e encoding.Encoder) CacheStore {
	return &fsCacheStore{
		directory: directory,
		fs:        f,
		encoder:   e,
	}
}

func (d *fsCacheStore) Read(vmName string) (config.VirtualMachine, error) {
	var newVM config.VirtualMachine

	path := d.vmFilePath(vmName)

	exists, err := d.fs.Exists(path)
	if err != nil {
		return newVM, fmt.Errorf("can't access cache file: %w", err)
	}

	if !exists {
		return newVM, os.ErrNotExist
	}

	data, err := d.fs.ReadFile(path)
	if err != nil {
		return newVM, err
	}

	err = d.encoder.Decode(bytes.NewBuffer(data), &newVM)
	if err != nil {
		return newVM, err
	}

	return newVM, nil
}

func (d *fsCacheStore) vmFilePath(vmName string) string {
	return filepath.Join(d.directory, fmt.Sprintf("%s.json", vmName))
}

func (d *fsCacheStore) Write(vm config.VirtualMachine) error {
	buf := new(bytes.Buffer)

	err := d.encoder.Encode(vm, buf)
	if err != nil {
		return err
	}

	return d.fs.WriteFile(d.vmFilePath(vm.Name), buf.Bytes(), cacheFileMode)
}

func (d *fsCacheStore) Delete(vm config.VirtualMachine) error {
	return d.fs.Remove(d.vmFilePath(vm.Name))
}

type cacheProviderDecorator struct {
	logger   logging.Logger
	internal Provider
	store    CacheStore
}

func newCacheProviderDecorator(cfg config.Global, logger logging.Logger, internal Provider) Provider {
	cacheConfig := cfg.ProviderCache

	if cacheConfig == nil || !cacheConfig.Enabled {
		return internal
	}

	return &cacheProviderDecorator{
		logger:   logger,
		internal: internal,
		store:    newFSCacheStorage(cacheConfig.Directory, fs.NewOS(), encoding.NewJSON()),
	}
}

func (c *cacheProviderDecorator) Get(ctx context.Context, vmName string) (config.VirtualMachine, error) {
	vm, err := c.store.Read(vmName)
	if err == nil {
		return vm, nil
	}

	log := c.logger.WithField("vm-name", vmName)

	readLog := log.WithError(err)
	writeReadLog := readLog.Warning
	if errors.Is(err, os.ErrNotExist) {
		writeReadLog = readLog.Debug
	}

	writeReadLog("Error while trying to read cache of the VM metadata")

	vm, err = c.internal.Get(ctx, vmName)
	if err != nil {
		return config.VirtualMachine{}, err
	}

	err = c.store.Write(vm)
	if err != nil {
		log.WithError(err).
			Error("Error while trying to write cache of the VM metadata")
	}

	return vm, nil
}

func (c *cacheProviderDecorator) Create(ctx context.Context, e executors.Executor, vm *config.VirtualMachine) error {
	err := c.internal.Create(ctx, e, vm)
	if err != nil {
		return err
	}

	err = c.store.Write(*vm)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("vm-name", vm.Name).
			Error("Error while trying to write cache of the VM metadata")
	}

	return nil
}

func (c *cacheProviderDecorator) Delete(ctx context.Context, vm config.VirtualMachine) error {
	err := c.internal.Delete(ctx, vm)
	if err != nil {
		return err
	}

	err = c.store.Delete(vm)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("vm-name", vm.Name).
			Error("Error while trying to write cache of the VM metadata")
	}

	return nil
}
