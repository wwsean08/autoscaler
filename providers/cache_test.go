package providers

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/assertions"
	encodingMocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/mocks/encoding"
	fsMocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/mocks/fs"
	loggingMocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/mocks/logging"
	executorMocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/executors"
	providerMocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/providers"
)

type fsCacheStoreTestCase struct {
	mockFS        func(f *fsMocks.FS, expectedPath string, testContent []byte)
	mockEncoder   func(t *testing.T, e *encodingMocks.Encoder, vm *config.VirtualMachine, testContent []byte)
	expectedError error
}

type fsCacheStoreTestCases map[string]fsCacheStoreTestCase

func testFSCacheStore(t *testing.T, tests fsCacheStoreTestCases, testCall func(s CacheStore, vm *config.VirtualMachine) error) {
	testDirectory := "test-directory"
	testVMName := "test-vm"
	vm := &config.VirtualMachine{Name: testVMName}
	testContent := []byte("test")

	expectedPath := filepath.Join(testDirectory, fmt.Sprintf("%s.json", testVMName))

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			fs := new(fsMocks.FS)
			defer fs.AssertExpectations(t)

			encoder := new(encodingMocks.Encoder)
			defer encoder.AssertExpectations(t)

			testCase.mockFS(fs, expectedPath, testContent)

			if testCase.mockEncoder != nil {
				testCase.mockEncoder(t, encoder, vm, testContent)
			}

			s := newFSCacheStorage(testDirectory, fs, encoder)
			err := testCall(s, vm)
			assertions.ErrorIs(t, err, testCase.expectedError)
		})
	}
}

func TestFSCacheStore_Read(t *testing.T) {
	testError := errors.New("test-error")

	mockFileExist := func(f *fsMocks.FS, expectedPath string) {
		f.On("Exists", expectedPath).
			Return(true, nil).
			Once()
	}

	mockSuccessfulRead := func(f *fsMocks.FS, expectedPath string, testContent []byte) {
		f.On("ReadFile", expectedPath).
			Return(testContent, nil).
			Once()
	}

	mockDecoding := func(e *encodingMocks.Encoder, testContent []byte, err error) {
		matchBytes := mock.MatchedBy(func(b *bytes.Buffer) bool {
			return bytes.Compare(b.Bytes(), testContent) == 0
		})

		e.On("Decode", matchBytes, mock.Anything).
			Return(err).
			Once()
	}

	tests := fsCacheStoreTestCases{
		"file doesn't exist": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				f.On("Exists", expectedPath).
					Return(false, nil).
					Once()
			},
			expectedError: os.ErrNotExist,
		},
		"file exist check returns an error": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				f.On("Exists", expectedPath).
					Return(false, testError).
					Once()
			},
			expectedError: testError,
		},
		"file exists but read fails": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				mockFileExist(f, expectedPath)
				f.On("ReadFile", expectedPath).
					Return(nil, testError).
					Once()
			},
			expectedError: testError,
		},
		"read succeeds but decoding fails": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				mockFileExist(f, expectedPath)
				mockSuccessfulRead(f, expectedPath, testContent)
			},
			mockEncoder: func(t *testing.T, e *encodingMocks.Encoder, vm *config.VirtualMachine, testContent []byte) {
				mockDecoding(e, testContent, testError)
			},
			expectedError: testError,
		},
		"cache properly read and decoded": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				mockFileExist(f, expectedPath)
				mockSuccessfulRead(f, expectedPath, testContent)
			},
			mockEncoder: func(t *testing.T, e *encodingMocks.Encoder, vm *config.VirtualMachine, testContent []byte) {
				mockDecoding(e, testContent, nil)
			},
			expectedError: nil,
		},
	}

	testFSCacheStore(t, tests, func(s CacheStore, vm *config.VirtualMachine) error {
		newVM, err := s.Read(vm.Name)
		vm = &newVM

		return err
	})
}

func TestFSCacheStore_Write(t *testing.T) {
	testError := errors.New("test-error")

	mockProperEncoding := func(t *testing.T, e *encodingMocks.Encoder, vm *config.VirtualMachine, testContent []byte) {
		e.On("Encode", *vm, mock.Anything).
			Run(func(a mock.Arguments) {
				buf, ok := a.Get(1).(*bytes.Buffer)
				require.True(t, ok)

				_, err := buf.Write(testContent)
				require.NoError(t, err)
			}).
			Return(nil).
			Once()
	}

	tests := fsCacheStoreTestCases{
		"encoder fails": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {},
			mockEncoder: func(t *testing.T, e *encodingMocks.Encoder, vm *config.VirtualMachine, testContent []byte) {
				e.On("Encode", *vm, mock.Anything).
					Return(testError).
					Once()
			},
			expectedError: testError,
		},
		"encoder succeeds but filesystem write fails": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				f.On("WriteFile", expectedPath, testContent, cacheFileMode).
					Return(testError).
					Once()
			},
			mockEncoder:   mockProperEncoding,
			expectedError: testError,
		},
		"cache properly encoded and saved": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				f.On("WriteFile", expectedPath, testContent, cacheFileMode).
					Return(nil).
					Once()
			},
			mockEncoder:   mockProperEncoding,
			expectedError: nil,
		},
	}

	testFSCacheStore(t, tests, func(s CacheStore, vm *config.VirtualMachine) error {
		return s.Write(*vm)
	})
}

func TestFSCacheStore_Delete(t *testing.T) {
	testError := errors.New("test-error")

	tests := fsCacheStoreTestCases{
		"filesystem remove fails": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				f.On("Remove", expectedPath).
					Return(testError).
					Once()
			},
			expectedError: testError,
		},
		"filesystem remove works properly": {
			mockFS: func(f *fsMocks.FS, expectedPath string, testContent []byte) {
				f.On("Remove", expectedPath).
					Return(nil).
					Once()
			},
			expectedError: nil,
		},
	}

	testFSCacheStore(t, tests, func(s CacheStore, vm *config.VirtualMachine) error {
		return s.Delete(*vm)
	})
}

func TestNewCacheProviderDecorator(t *testing.T) {
	tests := map[string]struct {
		cfg          config.Global
		expectedType interface{}
	}{
		"ProviderCache configuration is not defined": {
			cfg:          config.Global{},
			expectedType: &providerMocks.Provider{},
		},
		"ProviderCache is disabled": {
			cfg: config.Global{
				ProviderCache: &config.ProviderCache{
					Enabled: false,
				},
			},
			expectedType: &providerMocks.Provider{},
		},
		"ProviderCache is enabled": {
			cfg: config.Global{
				ProviderCache: &config.ProviderCache{
					Enabled: true,
				},
			},
			expectedType: &cacheProviderDecorator{},
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			c := newCacheProviderDecorator(testCase.cfg, nil, new(providerMocks.Provider))
			assert.IsType(t, testCase.expectedType, c)
		})
	}
}

type cacheProviderDecoratorTestCase struct {
	mockInternalProvider func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine)
	mockStore            func(s *providerMocks.CacheStore, v *config.VirtualMachine)
	mockLogger           func(l *loggingMocks.Logger, v *config.VirtualMachine)
	expectedError        error
}

type cacheProviderDecoratorTestCases map[string]cacheProviderDecoratorTestCase

func testCacheProviderDecorator(t *testing.T, tests cacheProviderDecoratorTestCases, testCall func(ctx context.Context, e executors.Executor, v *config.VirtualMachine, p *cacheProviderDecorator) error) {
	ctx := context.Background()
	executor := new(executorMocks.Executor)
	vmPtr := &config.VirtualMachine{
		Name: "vm",
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			internal := new(providerMocks.Provider)
			defer internal.AssertExpectations(t)

			store := new(providerMocks.CacheStore)
			defer store.AssertExpectations(t)

			testCase.mockInternalProvider(internal, ctx, executor, vmPtr)
			testCase.mockStore(store, vmPtr)

			p := &cacheProviderDecorator{
				internal: internal,
				store:    store,
			}

			if testCase.mockLogger != nil {
				logger := new(loggingMocks.Logger)
				defer logger.AssertExpectations(t)
				testCase.mockLogger(logger, vmPtr)

				p.logger = logger
			}

			err := testCall(ctx, executor, vmPtr, p)
			assertions.ErrorIs(t, err, testCase.expectedError)
		})
	}
}

func mockLoggerStoreError(l *loggingMocks.Logger, v *config.VirtualMachine, err error, withName bool) {
	if withName {
		l.On("WithField", "vm-name", v.Name).
			Return(l).
			Once()
	}

	l.On("WithError", err).
		Return(l).
		Once()

	l.On("Error", mock.Anything).
		Once()
}

func TestCacheProviderDecorator_Get(t *testing.T) {
	storeReadError := errors.New("store read error")
	testError := errors.New("test-error")

	mockLoggerUsage := func(l *loggingMocks.Logger, v *config.VirtualMachine) {
		l.On("WithField", "vm-name", v.Name).
			Return(l).
			Once()
		l.On("WithError", storeReadError).
			Return(l).
			Once()
		l.On("Warning", mock.Anything).
			Once()
	}

	tests := cacheProviderDecoratorTestCases{
		"store read succeeds": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Read", v.Name).
					Return(*v, nil).
					Once()
			},
			expectedError: nil,
		},
		"store read fails and internal get also fails": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Get", ctx, v.Name).
					Return(config.VirtualMachine{}, testError).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Read", v.Name).
					Return(config.VirtualMachine{}, storeReadError).
					Once()
			},
			mockLogger:    mockLoggerUsage,
			expectedError: testError,
		},
		"internal get succeeds but store write fails": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Get", ctx, v.Name).
					Return(*v, nil).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Read", v.Name).
					Return(config.VirtualMachine{}, storeReadError).
					Once()
				s.On("Write", *v).
					Return(testError).
					Once()
			},
			mockLogger: func(l *loggingMocks.Logger, v *config.VirtualMachine) {
				mockLoggerUsage(l, v)
				mockLoggerStoreError(l, v, testError, false)
			},
			expectedError: nil,
		},
		"cache is saved properly": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Get", ctx, v.Name).
					Return(*v, nil).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Read", v.Name).
					Return(config.VirtualMachine{}, storeReadError).
					Once()
				s.On("Write", *v).
					Return(nil).
					Once()
			},
			mockLogger:    mockLoggerUsage,
			expectedError: nil,
		},
	}

	testCacheProviderDecorator(t, tests, func(ctx context.Context, e executors.Executor, v *config.VirtualMachine, p *cacheProviderDecorator) error {
		_, err := p.Get(ctx, v.Name)
		return err
	})
}

func TestCacheProviderDecorator_Create(t *testing.T) {
	testError := errors.New("test-error")

	tests := cacheProviderDecoratorTestCases{
		"internal create fails": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Create", ctx, e, v).
					Return(testError).
					Once()
			},
			mockStore:     func(s *providerMocks.CacheStore, v *config.VirtualMachine) {},
			expectedError: testError,
		},
		"internal create succeeds but store create fails": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Create", ctx, e, v).
					Return(nil).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Write", *v).
					Return(testError).
					Once()
			},
			mockLogger: func(l *loggingMocks.Logger, v *config.VirtualMachine) {
				mockLoggerStoreError(l, v, testError, true)
			},
			expectedError: nil,
		},
		"cache is saved properly": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Create", ctx, e, v).
					Return(nil).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Write", *v).
					Return(nil).
					Once()
			},
			expectedError: nil,
		},
	}

	testCacheProviderDecorator(t, tests, func(ctx context.Context, e executors.Executor, v *config.VirtualMachine, p *cacheProviderDecorator) error {
		return p.Create(ctx, e, v)
	})
}

func TestCacheProviderDecorator_Delete(t *testing.T) {
	testError := errors.New("test-error")

	tests := cacheProviderDecoratorTestCases{
		"internal delete fails": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Delete", ctx, *v).
					Return(testError).
					Once()
			},
			mockStore:     func(s *providerMocks.CacheStore, v *config.VirtualMachine) {},
			expectedError: testError,
		},
		"internal delete succeeds but store delete fails": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Delete", ctx, *v).
					Return(nil).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Delete", *v).
					Return(testError).
					Once()
			},
			mockLogger: func(l *loggingMocks.Logger, v *config.VirtualMachine) {
				mockLoggerStoreError(l, v, testError, true)
			},
			expectedError: nil,
		},
		"cache is deleted properly": {
			mockInternalProvider: func(p *providerMocks.Provider, ctx context.Context, e executors.Executor, v *config.VirtualMachine) {
				p.On("Delete", ctx, *v).
					Return(nil).
					Once()
			},
			mockStore: func(s *providerMocks.CacheStore, v *config.VirtualMachine) {
				s.On("Delete", *v).
					Return(nil).
					Once()
			},
			expectedError: nil,
		},
	}

	testCacheProviderDecorator(t, tests, func(ctx context.Context, e executors.Executor, v *config.VirtualMachine, p *cacheProviderDecorator) error {
		return p.Delete(ctx, *v)
	})
}
