package providers

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/assertions"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/factories"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging/test"
	mocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/providers"
)

var (
	testFactoryType  = "provider mock"
	testProviderName = "test-provider"
	testProvider     = new(mocks.Provider)
	errTest          = errors.New("test-error")

	testFactory = func(cfg config.Global, logger logging.Logger) (Provider, error) {
		return testProvider, nil
	}

	testFactoryWithError = func(cfg config.Global, logger logging.Logger) (Provider, error) {
		return nil, errTest
	}
)

func mockFactoriesRegistry() func() {
	oldFactoriesRegistry := factoriesRegistry
	cleanup := func() {
		factoriesRegistry = oldFactoriesRegistry
	}

	factoriesRegistry = factories.NewRegistry(testFactoryType)

	return cleanup
}

func TestMustRegister(t *testing.T) {
	defer mockFactoriesRegistry()()

	assert.NotPanics(t, func() {
		MustRegister(testProviderName, testFactory)
	})

	assert.True(t, factoriesRegistry.IsRegistered(testProviderName))
}

func TestMustRegister_DoubledRegistration(t *testing.T) {
	defer mockFactoriesRegistry()()

	MustRegister(testProviderName, testFactory)

	assert.Panics(t, func() {
		MustRegister(testProviderName, testFactory)
	})

	assert.True(t, factoriesRegistry.IsRegistered(testProviderName))
}

func TestFactorize(t *testing.T) {
	defer mockFactoriesRegistry()()

	require.NotPanics(t, func() {
		MustRegister(testProviderName, testFactory)
	})

	cfg := config.Global{
		Provider: testProviderName,
	}
	logger := test.NewNullLogger()

	provider, err := Factorize(cfg, logger)
	assert.Equal(t, testProvider, provider)
	assert.NoError(t, err)
}

func TestFactorize_FactoryError(t *testing.T) {
	defer mockFactoriesRegistry()()

	require.NotPanics(t, func() {
		MustRegister(testProviderName, testFactoryWithError)
	})

	cfg := config.Global{
		Provider: testProviderName,
	}
	logger := test.NewNullLogger()

	provider, err := Factorize(cfg, logger)
	assert.Nil(t, provider)
	assertions.ErrorIs(t, err, errTest)
}

func TestFactorize_UnknownProvider(t *testing.T) {
	defer mockFactoriesRegistry()()

	cfg := config.Global{
		Provider: testProviderName,
	}
	logger := test.NewNullLogger()

	provider, err := Factorize(cfg, logger)
	assert.Nil(t, provider)
	assert.Equal(t, factories.NewErrNotRegistered(testFactoryType, testProviderName), err)
}
