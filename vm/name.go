package vm

import (
	"crypto/sha256"
	"fmt"
	"strings"
)

type NameRunnerData struct {
	RunnerShortToken string
	ProjectURL       string
	PipelineID       int64
	JobID            int64
}

func NewName(vmTag string, runnerData NameRunnerData) string {
	url := fmt.Sprintf("%s/%d/%d", runnerData.ProjectURL, runnerData.PipelineID, runnerData.JobID)
	hash := fmt.Sprintf("%x", sha256.Sum256([]byte(url)))

	parts := []string{
		"runner",
		runnerData.RunnerShortToken,
		vmTag,
		hash[:20],
	}

	return strings.ToLower(strings.Join(parts, "-"))
}
