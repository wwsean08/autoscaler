package vm

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewName(t *testing.T) {
	testTag := "test-mAchInE-tag"
	runnerData := NameRunnerData{
		RunnerShortToken: "a1b2C3D4",
		ProjectURL:       "https://gitlab.example.com/namespace/project",
		PipelineID:       1234,
		JobID:            4321,
	}

	expectedName := "runner-a1b2c3d4-test-machine-tag-8838f262c67ed8272ad9"

	assert.Equal(t, expectedName, NewName(testTag, runnerData))
}
