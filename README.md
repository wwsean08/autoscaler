# Autoscaler

Autoscaler is a driver for [GitLab Runner's Custom Executor](https://docs.gitlab.com/runner/executors/custom.html)
that implements an autoscaling algorithm.

**The project is very young and still under a heavy development.**

## Release channels

The driver is released in the form of a binary and is available from our S3 bucket.

For the latest beta version, based on the current state of `master` branch,
visit <https://gitlab-runner-custom-autoscaler-downloads.s3.amazonaws.com/master/index.html>.

For specific, tagged versions, please replace the `master` in the URL with a specific tag. For example,
to download version `v0.1.0`, go to `https://gitlab-runner-custom-autoscaler-downloads.s3.amazonaws.com/v0.1.0/index.html`.

All stable and RC releases, together with links to the specific S3 bucket, will be listed
on the [Releases](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/-/releases)
page.

## Roadmap

### First iteration

1. Implement initial implementation for creating/deleting a VM and provisioning
   some method of script execution. We will start with GCP, Windows and
   execution via WinRM, but the architecture should allow extensions in the future.

1. Split the mechanism to three independent steps: a) create VM, b) execute
   arbitrary script on a VM, c) delete the VM.

1. Implement CLI interface and integration with Custom Executor

1. First, little performance improvements (API usage).

### Next iterations (TODO)

1. Provision Docker Engine and return Docker credentials in the output (a future
   replacement for Docker Machine executor).

1. Implement a daemon that would manage machines and create them in advance,
   like Docker Machine executor of GitLab Runner does now.

1. Support other popular cloud providers: AWS, DigitalOcean, Azure.

## Documentation

Documentation is found in [docs/README.md](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/tree/master/docs/README.md).

## License

MIT

## Author

GitLab, 2019
