package config

import (
	gcp "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
)

type VirtualMachine struct {
	Name string

	IPAddress string

	// For WinRM executor
	Username string
	Password string

	GCP gcp.VirtualMachine
}
