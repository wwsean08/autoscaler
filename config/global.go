package config

import (
	"fmt"
	"io/ioutil"

	"github.com/BurntSushi/toml"

	winrm "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors/winrm/config"
	gcp "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
)

type Global struct {
	Provider string
	Executor string
	OS       string

	ProviderCache *ProviderCache

	LogLevel  string
	LogFile   string
	LogFormat string

	VMTag string

	GCP gcp.Provider

	WinRM winrm.Executor
}

type ProviderCache struct {
	Enabled   bool
	Directory string
}

func LoadFromFile(file string) (Global, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return Global{}, fmt.Errorf("couldn't read configuration file %q: %w", file, err)
	}

	var cfg Global

	err = toml.Unmarshal(data, &cfg)
	if err != nil {
		return Global{}, fmt.Errorf("couldn't parse TOML content of the configuration file: %w", err)
	}

	return cfg, nil
}
