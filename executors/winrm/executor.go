package winrm

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"time"

	"github.com/masterzen/winrm"

	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors/winrm/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
)

const (
	Name = "winrm"

	defaultExecutionMaxRetries = 5

	port = 5985

	powershellCmd = "powershell.exe -NoProfile -NonInteractive -NoLogo -InputFormat text -OutputFormat text -ExecutionPolicy Bypass -Command -"
)

var initWinRMScript = `Write-Host "Starting WinRM configuration..."
winrm quickconfig -q -force
winrm set winrm/config/service/Auth '@{Basic="true"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="1024"}'
New-NetFirewallRule -DisplayName "Allow inbound WinRM" -Direction Inbound -LocalPort 5985-5986 -Protocol TCP -Action Allow
Write-Host "WinRM should be ready to use!"
`

type Client interface {
	RunWithInput(command string, stdout, stderr io.Writer, stdin io.Reader) (int, error)
}

var (
	availabilityCheckTimeout = 10 * time.Second

	newClient = func(endpoint *winrm.Endpoint, user string, password string) (Client, error) {
		return winrm.NewClient(endpoint, user, password)
	}

	errWinRMServicePortUnavailable = errors.New("WinRM service port is unavailable")
	errWinRMServiceUnavailable     = errors.New("WinRM service is unavailable")
)

type Executor struct {
	config config.Executor
	logger logging.Logger
	port   int
}

func New(cfg globalConfig.Global, logger logging.Logger) (executors.Executor, error) {
	e := &Executor{
		config: cfg.WinRM,
		logger: logger,
		port:   port,
	}

	return e, nil
}

func (e *Executor) Execute(ctx context.Context, vm globalConfig.VirtualMachine, script []byte) error {
	endpoint := winrm.NewEndpoint(
		vm.IPAddress,
		e.port,
		false,
		false,
		nil,
		nil,
		nil,
		time.Duration(e.config.MaximumTimeout)*time.Second,
	)

	log := e.logger.WithField("vm-name", vm.Name)
	log.Info("Executing script on the Virtual Machine...")

	client, err := newClient(endpoint, vm.Username, vm.Password)
	if err != nil {
		return fmt.Errorf("couldn't create a WinRM client for Virtual Machine %q: %w", vm.Name, err)
	}

	err = e.checkWinRMPortAvailability(log, endpoint)
	if err != nil {
		return err
	}

	err = e.checkWinRMServiceAvailability(log, vm, client)
	if err != nil {
		return err
	}

	err = e.runScript(client, vm, script, os.Stdout, os.Stdout)
	if err != nil {
		return runner.NewBuildFailureError(err) // Trigger "Build failure" on Runner!
	}

	log.Info("Executed script on the Virtual Machine")

	return nil
}

func (e *Executor) checkWinRMPortAvailability(logger logging.Logger, endpoint *winrm.Endpoint) error {
	address := fmt.Sprintf("%s:%d", endpoint.Host, endpoint.Port)

	for i := 0; i < e.executionMaxRetries(); i++ {
		log := logger.
			WithField("retry-count", i).
			WithField("address", address)
		log.Info("Checking availability of WinRM service port...")

		conn, err := net.DialTimeout("tcp", address, 1*time.Second)
		if err == nil {
			log.Debug("WinRM service port available")
			_ = conn.Close()

			return nil
		}

		nerr, ok := err.(net.Error)
		if ok && nerr.Timeout() {
			log.
				WithField("ok", ok).
				WithError(nerr).
				Debug("Command timed out, retrying in 10s")
		} else {
			log.
				WithField("ok", ok).
				WithError(err).
				Debug("unknown error, retrying in 10s")
		}

		sleep := availabilityCheckTimeout
		log.
			WithField("retry-interval", sleep).
			Debug("Waiting before next retry")

		time.Sleep(sleep)
	}

	return errWinRMServicePortUnavailable
}

func (e *Executor) executionMaxRetries() int {
	if e.config.ExecutionMaxRetries > 0 {
		return e.config.ExecutionMaxRetries
	}

	return defaultExecutionMaxRetries
}

func (e *Executor) checkWinRMServiceAvailability(logger logging.Logger, vm globalConfig.VirtualMachine, client Client) error {
	for i := 0; i < e.executionMaxRetries(); i++ {
		log := logger.WithField("retry-count", i)
		log.Info("Checking availability of WinRM service...")

		err := e.runScript(client, vm, nil, ioutil.Discard, ioutil.Discard)
		if err == nil {
			log.Debug("WinRM service available")

			return nil
		}

		sleep := availabilityCheckTimeout
		log.
			WithError(err).
			WithField("retry-interval", sleep).
			Warning("Waiting before next retry")

		time.Sleep(sleep)
	}

	return errWinRMServiceUnavailable
}

func (e *Executor) runScript(client Client, vm globalConfig.VirtualMachine, script []byte, stdout io.Writer, stderr io.Writer) error {
	// Appending `exit` command at the end, to be 100% sure that the script
	// will close the shell - which means it will finish the execution - after
	// the last defined user command is finishedW
	script = append(script, []byte("\n\nexit\n")...)

	exitCode, err := client.RunWithInput(powershellCmd, stdout, stderr, bytes.NewBuffer(script))
	if err != nil {
		return fmt.Errorf("couldn't run WinRM command on the Virtual Machine %q: %w", vm.Name, err)
	}

	if exitCode != 0 {
		return fmt.Errorf("script execution on the Virtual Machine %q exited with %d error code", vm.Name, exitCode)
	}

	return nil
}

func (e *Executor) ProvisionScript() string {
	return initWinRMScript
}
