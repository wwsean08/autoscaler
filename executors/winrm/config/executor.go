package config

type Executor struct {
	MaximumTimeout      int
	ExecutionMaxRetries int
}
