package custom

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func NewConfigCommand() cli.Command {
	cmd := new(ConfigCommand)
	cmd.abstractCustomCommand.customCommand = cmd

	return cli.Command{
		Handler: cmd,
		Config: cli.Config{
			Name:    "config",
			Aliases: []string{"co"},
			Usage:   "Provide some configuration details for Custom Executor",
			Description: `
This is the implementation of Config stage of the Custom Executor.

This command is providing some configuration details that can be next used
by Runner's Custom Executor.

Currently, this command will pass the hostname of created Virtual Machine, to be
stored with Runner's 'Build' metadata, which can be next used e.g. to print the
hostname of the VM in the job's trace.

This command requires GitLab Runner 12.4 or higher to work properly.

Details about how this command is used can be found at
https://docs.gitlab.com/runner/executors/custom.html#config.`,
		},
	}
}

type ConfigCommand struct {
	abstractCustomCommand

	cfg    config.Global
	logger logging.Logger

	vmName string
}

func (c *ConfigCommand) CustomExecute(ctx *cli.Context) error {
	err := c.init(ctx)
	if err != nil {
		return fmt.Errorf("couldn't initialize ConfigCommand: %w", err)
	}

	c.logger.Info("Executing the command")

	err = runner.GetAdapter().WriteCustomExecutorConfig(os.Stdout, c.vmName)
	if err != nil {
		return fmt.Errorf("couldn't write JSON output for the ConfigCommand")
	}

	return nil
}

func (c *ConfigCommand) init(ctx *cli.Context) error {
	c.cfg = ctx.Config()
	c.logger = ctx.
		Logger().
		WithField("command", "config_exec")

	runnerData := vm.NameRunnerData{
		RunnerShortToken: runner.GetAdapter().ShortToken(),
		ProjectURL:       runner.GetAdapter().ProjectURL(),
		PipelineID:       runner.GetAdapter().PipelineID(),
		JobID:            runner.GetAdapter().JobID(),
	}

	c.vmName = vm.NewName(c.cfg.VMTag, runnerData)

	return nil
}
