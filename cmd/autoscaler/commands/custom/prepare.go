package custom

import (
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func NewPrepareCommand() cli.Command {
	cmd := new(PrepareCommand)
	cmd.abstractCustomCommand.customCommand = cmd

	return cli.Command{
		Handler: cmd,
		Config: cli.Config{
			Name:    "prepare",
			Aliases: []string{"p"},
			Usage:   "Prepare the environment for Custom Executor",
			Description: `
This is the implementation of Prepare stage of the Custom Executor.

This command is creating the Virtual Machine, which will be next used
to execute the scripts.

Details about how this command is used can be found at
https://docs.gitlab.com/runner/executors/custom.html#prepare.`,
		},
	}
}

type PrepareCommand struct {
	abstractCustomCommand

	cfg    config.Global
	logger logging.Logger

	vmName   string
	provider providers.Provider
	executor executors.Executor
}

func (c *PrepareCommand) CustomExecute(ctx *cli.Context) error {
	err := c.init(ctx)
	if err != nil {
		return fmt.Errorf("couldn't initialize PrepareCommand: %w", err)
	}

	c.logger.Info("Executing the command")

	vmCfg := config.VirtualMachine{
		Name: c.vmName,
	}

	fmt.Println("Creating virtual machine for the job...")

	err = c.provider.Create(ctx.Ctx, c.executor, &vmCfg)
	if err != nil {
		return fmt.Errorf("couldn't create the Virtual Machine %q: %w", c.vmName, err)
	}

	fmt.Println("Virtual machine created!")

	return nil
}

func (c *PrepareCommand) init(ctx *cli.Context) error {
	c.cfg = ctx.Config()
	c.logger = ctx.
		Logger().
		WithField("command", "prepare_exec")

	var err error

	runnerData := vm.NameRunnerData{
		RunnerShortToken: runner.GetAdapter().ShortToken(),
		ProjectURL:       runner.GetAdapter().ProjectURL(),
		PipelineID:       runner.GetAdapter().PipelineID(),
		JobID:            runner.GetAdapter().JobID(),
	}

	c.vmName = vm.NewName(c.cfg.VMTag, runnerData)

	c.provider, err = providers.Factorize(c.cfg, c.logger)
	if err != nil {
		return fmt.Errorf("couldn't factorize provider: %w", err)
	}

	c.executor, err = executors.Factorize(c.cfg, c.logger)
	if err != nil {
		return fmt.Errorf("couldn't factorize executor: %w", err)
	}

	return nil
}
