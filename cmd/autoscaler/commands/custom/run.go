package custom

import (
	"errors"
	"fmt"
	"io/ioutil"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func NewRunCommand() cli.Command {
	cmd := new(RunCommand)
	cmd.abstractCustomCommand.customCommand = cmd

	return cli.Command{
		Handler: cmd,
		Config: cli.Config{
			Name:      "run",
			Aliases:   []string{"r"},
			Usage:     "Run the script from Custom Executor",
			ArgsUsage: "path_to_script stage_name",
			Description: `
This is the implementation of Run stage of the Custom Executor.

The command will execute several scripts (provided by the Runner) on the
Virtual Machine created previously by the 'prepare' command.

The command gets two arguments:

- path_to_script - which contains the path to the script that should be executed,
- stage_name - which contains the name of the job stage that is being executed.

Details about how this command is used can be found at https://docs.gitlab.com/runner/executors/custom.html#run.`,
		},
	}
}

type RunCommand struct {
	abstractCustomCommand

	cfg    config.Global
	logger logging.Logger

	vmName   string
	provider providers.Provider
	executor executors.Executor
}

func (c *RunCommand) CustomExecute(ctx *cli.Context) error {
	argsCnt := ctx.Cli.NArg()
	if argsCnt < 2 {
		return errors.New("missing required arguments")
	}

	err := c.init(ctx)
	if err != nil {
		return fmt.Errorf("couldn't initialize RunCommand: %w", err)
	}

	c.logger.Info("Executing the command")

	args := ctx.Cli.Args()
	scriptPath := args.Get(0)

	vmCfg, err := c.provider.Get(ctx.Ctx, c.vmName)
	if err != nil {
		return fmt.Errorf("couldn't get the VM %q details: %w", c.vmName, err)
	}

	script, err := ioutil.ReadFile(scriptPath)
	if err != nil {
		return fmt.Errorf("couldn't load the %q script: %w", scriptPath, err)
	}

	err = c.executor.Execute(ctx.Ctx, vmCfg, script)
	if err != nil {
		return fmt.Errorf("couldn't execute the script on the Virtual Machine %q: %w", c.vmName, err)
	}

	return nil
}

func (c *RunCommand) init(ctx *cli.Context) error {
	c.cfg = ctx.Config()
	c.logger = ctx.
		Logger().
		WithFields(logging.Fields{
			"command": "run_exec",
			"stage":   ctx.Cli.Args().Get(1),
		})

	var err error

	runnerData := vm.NameRunnerData{
		RunnerShortToken: runner.GetAdapter().ShortToken(),
		ProjectURL:       runner.GetAdapter().ProjectURL(),
		PipelineID:       runner.GetAdapter().PipelineID(),
		JobID:            runner.GetAdapter().JobID(),
	}

	c.vmName = vm.NewName(c.cfg.VMTag, runnerData)

	c.provider, err = providers.Factorize(c.cfg, c.logger)
	if err != nil {
		return fmt.Errorf("couldn't factorize provider: %w", err)
	}

	c.executor, err = executors.Factorize(c.cfg, c.logger)
	if err != nil {
		return fmt.Errorf("couldn't factorize executor: %w", err)
	}

	return nil
}
