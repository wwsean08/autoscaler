package selftest

import (
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
)

func NewLogCheckCommand() cli.Command {
	return cli.Command{
		Config: cli.Config{
			Name:    "log-check",
			Aliases: []string{"lc"},
			Usage:   "Allows to test logging configuration",
		},
		Handler: new(LogCheckCommand),
	}
}

type LogCheckCommand struct{}

func (c *LogCheckCommand) Execute(ctx *cli.Context) error {
	fmt.Println("Testing all logging levels:")

	log := ctx.Logger()

	defer func() {
		r := recover()
		fmt.Printf("Recovered from: %+v\n", r)

		log.Fatal("Fatal message")
	}()

	log.Debug("Debug message")
	log.Info("Info message")
	log.Warning("Warning message")
	log.Error("Error message")
	log.Panic("Panic message")

	return nil
}
