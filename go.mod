module gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/masterzen/winrm v0.0.0-20190308153735-1d17eaf15943
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/afero v1.2.2
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.21.0
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	gitlab.com/gitlab-org/gitlab-runner v12.4.2-0.20191106123436-fc03aa348bac+incompatible
	go.opencensus.io v0.20.2 // indirect
	golang.org/x/crypto v0.0.0-20190418165655-df01cb2cc480 // indirect
	google.golang.org/api v0.3.1
	google.golang.org/grpc v1.19.1 // indirect
)
